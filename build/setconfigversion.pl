#!/usr/bin/perl -w -i

while (<>) {
    s/^(\s+)VERSION(\s+)=> .*/$1VERSION$2=> "$ENV{VERSION}",/;
    print $_;
}
