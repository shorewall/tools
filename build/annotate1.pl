#! /usr/bin/perl
#
#  Annotate a configuration file -- (C) 2011 -- Tom Eastep (teastep@shorewall.net)
#
#     This program is under GPL [http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt]
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of Version 2 of the GNU General Public License
#	as published by the Free Software Foundation.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Usage:
#
#   Annotate <config file> <test version of manpage>
#
#   The annotated config file is written to standard out
########################################################################################
use strict;

my ( $confname, $txtname ) = @ARGV;

my ( $conf, $txt );

my %options;

sub fatal_error {
    die "   ERROR: @_";
}

fatal_error( "usage: $0 <config file> <text manpage>" ) unless @ARGV == 2;


open $conf, '<', $confname or fatal_error "Unable to open $confname: $!";
open $txt,  '<', $txtname  or fatal_error "Unable to open $txtname: $!";

while ( <$txt> ) {
    last if /^Description/;
}

while ( <$conf> ) {
    next if /^\w*$/;
    print $_;
    last if /^###/;
}

my $separator = $_;

while ( <$txt> ) {
    last if /^FILES/;
#    s/^/          / unless /^ /;
    print "# $_";
}

print "$separator";

while ( <$conf> ) {
    next if /^\w*$/;
    print "$_";
}

close $txt;
close $conf;






