#!/bin/sh
#
#  Upload a release to the master server
#
#  (C) 2007 -- Tom Eastep (teastep@shorewall.net)
#
#
#     This program is under GPL [http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt]
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of Version 2 of the GNU General Public License
#	as published by the Free Software Foundation.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Usage:
#
#       upload [-pclL6sxh] <version>
#

usage() {
    echo "Usage: upload [-pclL6sxh] <version>"
}

rpm=
rpm6=
literpm=
lite6rpm=
perlrpm=
shellrpm=

DEST=
USER="webadmin@mail.shorewall.net"
SHOREWALL=shorewall
COMMAND=scp

UPLOADCOMMON=
UPLOADPERL=
UPLOADSHELL=
UPLOADLITE=
UPLOADXML=
UPLOADHTML=
UPLOAD6=
UPLOAD6LITE=
PARTIAL=Yes
BASE=

done=

case $1 in
    -*)
	;;
    *)
	UPLOADCOMMON=Yes
	UPLOADPERL=Yes
	UPLOADSHELL=Yes
	UPLOADLITE=Yes
	UPLOADXML=Yes
	UPLOADHTML=Yes

	case $1 in
	    4.0.*)
		;;
	    *)
		UPLOAD6=Yes
		UPLOAD6LITE=Yes
		;;
	esac

	PARTIAL=
	done=Yes
	;;
esac

while [ -z "$done" ]; do
    [ $# -eq 0 ] && break

    option=$1
    case $option in
	-*)
	    option=${option#-}

	    [ -z "$option" ] && break

	    while [ -n "$option" ]; do
		case $option in
		    p*)
			UPLOADPERL=Yes
			option=${option#p}
			;;
		    c*)
			UPLOADCOMMON=Yes
			option=${option#c}
			;;
		    s*)
			UPLOADSHELL=Yes
			option=${option#s}
			;;
		    l*)
			UPLOADLITE=Yes
			option=${option#l}
			;;
		    x*)
			UPLOADXML=Yes
			option=${option#x}
			;;
		    h*)
			UPLOADHTML=Yes
			option=${option#h}
			;;
		    6*)
			UPLOAD6=Yes
			option=${option#6}
			;;
		    L*)
			UPLOAD6LITE=Yes
			option=${option#L}
			;;
	    	    *)
			usage
			;;
		esac
	    done
	    shift
	    ;;
	*)
	    done=Yes
	    ;;
    esac
done


case $1 in
    4.*Beta*|4.*RC*)
	DEST="/srv/ftp/pub/shorewall/development/${1%.*}/shorewall-$1"
	SHOREWALL=shorewall-common
	rpm=shorewall-common-${1%-*}-0${1#*-}.noarch.rpm
	rpm6=shorewall6-${1%-*}-0${1#*-}.noarch.rpm
	literpm=shorewall-lite-${1%-*}-0${1#*-}.noarch.rpm
	lite6rpm=shorewall6-lite-${1%-*}-0${1#*-}.noarch.rpm
	perlrpm=shorewall-perl-${1%-*}-0${1#*-}.noarch.rpm
	shellrpm=shorewall-shell-${1%-*}-0${1#*-}.noarch.rpm
	BASE=Yes
	;;
    4.[02].*.*)
	BASEVERSION=${1%.*}
	PATCHNUM=${1##*.}
	DEST="/srv/ftp/pub/shorewall/${BASEVERSION%.*}/shorewall-${BASEVERSION}"
	SHOREWALL=shorewall-common
	rpm=shorewall-common-${BASEVERSION}-${PATCHNUM}.noarch.rpm
	rpm6=shorewall6-${BASEVERSION}-${PATCHNUM}.noarch.rpm
	literpm=shorewall-lite-${BASEVERSION}-${PATCHNUM}.noarch.rpm
	lite6rpm=shorewall6-lite-${BASEVERSION}-${PATCHNUM}.noarch.rpm
	perlrpm=shorewall-perl-${BASEVERSION}-${PATCHNUM}.noarch.rpm
	shellrpm=shorewall-shell-${BASEVERSION}-${PATCHNUM}.noarch.rpm
	;;
    4.[02].*)
	DEST="/srv/ftp/pub/shorewall/${1%.*}/shorewall-$1"
	SHOREWALL=shorewall-common
	rpm=shorewall-common-${1}-0base.noarch.rpm
	rpm6=shorewall6-${1}-0base.noarch.rpm
	literpm=shorewall-lite-${1}-0base.noarch.rpm
	lite6rpm=shorewall6-lite-${1}-0base.noarch.rpm
	perlrpm=shorewall-perl-${1}-0base.noarch.rpm
	shellrpm=shorewall-shell-${1}-0base.noarch.rpm
	BASE=Yes
	;;
    4.3.*)
	DEST="/srv/ftp/pub/shorewall/development/${1%.*}/shorewall-$1"
	SHOREWALL=shorewall-common
	rpm=shorewall-common-${1}-0base.noarch.rpm
	literpm=shorewall-lite-${1}-0base.noarch.rpm
	perlrpm=shorewall-perl-${1}-0base.noarch.rpm
	shellrpm=shorewall-shell-${1}-0base.noarch.rpm
	rpm6=shorewall6-${1}-0base.noarch.rpm
	lite6rpm=shorewall6-lite-${1}-0base.noarch.rpm
	BASE=Yes
	;;
    *)
	echo "Unknown Version $1" >&2
	exit 1
	;;
esac

[ $# -eq 1 ] || { echo "Usage: $(basename $0) <version>"; exit 2; }

if [ -z "$PATCHNUM" -a -d shorewall-common ]; then
    echo "   ERROR: Paranoid to upload a base release when a patch release has been built in the current directory" >&2
    exit 1 
fi

[ -d errata ] || mkdir errata

[ -f known_problems.txt ] || echo "There are no known problems in Shorewall version $1" > known_problems.txt

[ -n "$PARTIAL" -a -z "$UPLOADCOMMON" ]  && SHOREWALLDIR=shorewall-common || SHOREWALLDIR=$SHOREWALL-${1}

cp $SHOREWALLDIR/releasenotes.txt releasenotes.txt

if [ "$HOSTNAME" = lists ]; then
	mkdir -p $DEST
	[ -n "$BASE" ] && mkdir $DEST/base
	COMMAND=cp
else
	ssh $USER mkdir -p $DEST
	[ -n "$BASE" ] && ssh $USER mkdir -p $DEST/base
	DEST=${USER}:${DEST}
fi

files="errata known_problems.txt releasenotes.txt patch-*-$1 ${1}.*"
base=

[ -n "$UPLOAD6" ]      && base="$base shorewall6-${1}.tar.*"          && files="$files shorewall6-${1}.*          $rpm6"
[ -n "$UPLOADPERL" ]   && base="$base shorewall-perl-${1}.tar.*"      && files="$files shorewall-perl-${1}.*      $perlrpm"
[ -n "$UPLOADCOMMON" ] && base="$base shorewall-common-${1}.tar.*"    && files="$files shorewall-common-${1}.*    $rpm"
[ -n "$UPLOADSHELL" ]  && base="$base shorewall-shell-${1}.tar.*"     && files="$files shorewall-shell-${1}.*     $shellrpm"
[ -n "$UPLOADLITE" ]   && base="$base shorewall-lite-${1}.tar.*"      && files="$files shorewall-lite-${1}.*      $literpm"
[ -n "$UPLOAD6LITE" ]  && base="$base shorewall6-lite-${1}.tar.*"     && files="$files shorewall6-lite-${1}.*     $lite6rpm"
[ -n "$UPLOADXML" ]    && base="$base shorewall-docs-xml-${1}.tar.*"  && files="$files shorewall-docs-xml-${1}.*  $literpm"
[ -n "$UPLOADHTML" ]   && base="$base shorewall-docs-html-${1}.tar.*" && files="$files shorewall-docs-html-${1}.* $literpm"

[ -n "$BASE" ] && $COMMAND $base $DEST/base

$COMMAND -r $files $DEST
