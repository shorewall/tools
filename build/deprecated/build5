#!/bin/bash
#
# Shorewall Release Processing -- (C) 2003-2010 -- Tom Eastep (teastep@shorewall.net)
#                              -- (C) 2005,2006 -- Cristian Rodriguez (webmaster@shorewall.net)
# Version : $Id: buildshorewall 9189 2008-12-29 19:55:18Z teastep $
#
#     This program is under GPL [http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt]
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of Version 2 of the GNU General Public License
#	as published by the Free Software Foundation.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# I install this script in /usr/local/bin/makeshorewall.
#
# Usage:
#
#   makeshorewall [ -trhxclpsS ] <version> [ <previous version> ]
#
#      -t      Build tarball
#      -r      Build RPM
#      -c      Build Core
#      -s      Build Standard
#      -l      Build Lite
#      -L      Build 6 Lite
#      -6      Build 6
#      -i      Build Init
#      -h      Build HTML documentation
#      -x      Build XML documentation
#      -s      Build Giant Source Package
#
# If no options are given, all options are assumed.
#
# If <previous version> is given, patch files reflecting the differences
# between that version and the current version ( <version> ) are generated.
# The directory ./shorewall-<previous version> must exist and contain the
# version against which the patch is generated.
################################################################################
#                          C O N F I G U R A T I O N
################################################################################
#
# XSL Stylesheet to use for XML->HTML conversion
#
STYLESHEET=/usr/share/xml/docbook/stylesheet/nwalsh/current/xhtml/docbook.xsl
MACSTYLESHEET=/opt/local/share/xsl/docbook-xsl/xhtml/docbook.xsl
#
# Directory where the build log will be placed. The log has the name
# shorewall_build_<version>.log
#
LOGDIR=$PWD
#
# Your RPM build directory
#
RPMDIR=~/rpm/
#
# Directory where you want the release to be built -- must be fully-qualified
#
DIR=$PWD
#
# location and options for GnuPG
#
GPG="/usr/bin/gpg -ab --batch --comment 'To verify this, you can download our public key at https://lists.shorewall.net/shorewall.gpg.key'"
#
# SVN Repository
#
SVN=https://shorewall.svn.sourceforge.net/svnroot/shorewall
#
# GIT Repository
#
GIT=~/shorewall/trunk
################################################################################
#                               V A R I A B L E S
################################################################################
VERSION=
BASEVERSION=
OLDVERSION=
SHOREWALLDIR=
SHOREWALLLITEDIR=
SHOREWALLINITDIR=
SOURCEDIR=
RELEASEDIR=release
BRANCH=
FILE=
SVNBRANCH=
LITESVNBRANCH=
INITSVNBRANCH=
RELEASESVNBRANCH=release
XMLPROJ=
RPMNAME=
LITERPMNAME=
INITRPMNAME=
TARBALL=
LITETARBALL=
INITTARBALL=
LOGFILE=
HTMLDIR=
BUILDTARBALL=
BUILDRPM=
BUILDXML=
BUILDHTML=
BUILDSRC=
PERLDIR=
PERLBRANCH=
PERLRPMNAME=
PERLTARBALL=
BUILDCORE=
BUILDSTD
BUILDPERL=
BUILDLITE=
BUILDINIT=
PATCHRELEASE=
PATCHNUM=
BASEURL=http://www.shorewall.org

SVN6BRANCH=
LITE6SVNBRANCH=
SHOREWALL6DIR=
LITE6DIR=
BUILD6=
BUILD6LITE=
RPM6NAME=
LITE6RPMNAME=
TARBALL6=
LITE6TARBALL=

################################################################################
#                               F U N C T I O N S
################################################################################
progress_message()
{
    echo         >>    $LOGFILE
    echo "$@" | tee -a $LOGFILE
    echo         >>    $LOGFILE
}

report()
{
    echo "$@" | tee -a $LOGFILE
}

do_or_die()
{
    eval $@ || { progress_message "Step \"$*\" FAILED" ; exit 2; }
}

fatal_error() {
    progress_message "$*"
    exit 2
}

list_search() # $1 = element to search for , $2-$n = list
{
    local e
    e=$1

    while [ $# -gt 1 ]; do
	shift
	[ "x$e" = "x$1" ] && return 0
    done

    return 1
}

do_rpmbuild() {
    RPM=yes rpmbuild --target noarch-linux $@ >> $LOGFILE 2>&1
}

do_buildanrpm() {
    progress_message "Building ${1}..."

    cd /tmp
    rm -rf ${2}-${BASEVERSION}
    do_or_die "cp -a $DIR/${3} ${2}-${BASEVERSION}"
    do_or_die "tar -zcf $RPMDIR/SOURCES/${2}-${BASEVERSION}.tgz ${2}-${BASEVERSION}"
    cd $DIR
    do_or_die "cp ${3}/${2}.spec $RPMDIR/SPECS/"
    do_or_die do_rpmbuild "-ba $RPMDIR/SPECS/${2}.spec"
    do_or_die cp -a $RPMDIR/RPMS/noarch/${1} .
}

usage()
{
    echo "usage: $(basename $0) [ -trhxcl6L] <version> [ <old-version> ]"
    exit 2
}

do_export()
{
    local branch
    local file
    [ -f $GIT/.git/refs/heads/$BASEVERSION ] && branch=${BASEVERSION} || branch=master
    progress_message "Exporting $1 from Git branch $branch..." && do_or_die "git --git-dir=$GIT/.git archive --format=tar $branch $1 | tar -xf - >> $LOGFILE 2>&1"
    [ $1 = $2 ] || do_or_die "mv -f $1 $2 >> $LOGFILE 2>&1"
    for file in $2/install.sh $2/uninstall.sh $2/fallback.sh; do
	if [ -f $file ]; then
	    eval perl -p -i -e "'s/^VERSION=.*/VERSION=${VERSION}/'" $file || { progress_message "Can't set VERSION in $file" ; exit 2; }
	fi
    done
}

do_export_manapges()
{
    local branch
    local file
    [ -f $GIT/.git/refs/heads/$BASEVERSION ] && branch=${BASEVERSION} || branch=master
    progress_message "Exporting $1 from Git branch $branch..." && do_or_die "git --git-dir=$GIT/.git archive --format=tar $branch $1 | tar -xf - >> $LOGFILE 2>&1"
    do_or_die "mv -f $1/manpages $2 >> $LOGFILE 2>&1"
    rm -rf $1
}

do_manpages()
{
    cd manpages

    for f in *.xml; do
	case $f in
	    *template.xml)
		;;
	    *)
		progress_message "Generating Man Page from $f..."
		do_or_die "xmlto --skip-validation --stringparam man.base.url.for.relative.links=$BASEURL/manpages${2}/ man $f >> $LOGFILE 2>&1"
		case $f in
		    *.conf.*|*shorewall.xml|shorewall6.xml|shorewall-lite.xml|shorewall-init.xml|shorewall6-lite.xml)
                        ;;
		    *)
			g=${f#shorewall${1}-}
			h=$(ls ${g%.xml}.[58])
			f=shorewall${1}-$h
			mv $h $f
			;;
		esac
	    
		eval perl -p -w -i -e "'s|/manpages${2}/manpages${2}?|/manpages${2}|'" $f
		;;
	esac
    done

    rm -f *.bak

    cd ..
}

do_configfiles()
{
    pushd $1 > /dev/null

    local mandir=$2

    for f in *; do
	local xml
	local text
	
	if [ $f = shorewall${3}.conf ]; then
	    xml=$mandir/$f.xml
	    text=$f.txt
	else
	    xml=$mandir/shorewall${3}-$f.xml
	    text=shorewall${3}-$f.txt
	fi

	[ -f $xml ] || continue


	progress_message "Annotating $f..."
	do_or_die "xmlto --skip-validation txt $xml 2>> $LOGFILE"
	
	case $f in
	    *.conf)
		do_or_die "annotate.pl $f $text > $f.annotated 2>> $LOGFILE"
		;;
	    *)
		do_or_die "annotate1.pl $f $text > $f.annotated 2>> $LOGFILE"
		;;
	esac

	rm -f $text
    done

    popd > /dev/null
}

setperlversions() 
{
    local module
    local version

    export VERSION
    do_or_die setconfigversion.pl $SHOREWALLDIR/Perl/Shorewall/Config.pm

   if [ -f $RELEASEDIR/moduleversions ]; then
       while read module version; do
	   if [ -n "$MACHOST" ]; then
	       sed -i '' -e "s/MODULEVERSION/$version/" $SHOREWALLDIR/Perl/Shorewall/$module
	   else
	       sed -i "s/MODULEVERSION/$version/" $SHOREWALLDIR/Perl/Shorewall/$module
	   fi
       done < $RELEASEDIR/moduleversions
    fi
}

################################################################################
#                E X E C U T I O N   S T A R T S   H E R E
################################################################################

set -e
set -u

MACHOST=

case $(uname) in
    Darwin)
        MACHOST=Yes
	STYLESHEET=$MACSTYLESHEET
	;;
esac

done=

[ $# -eq 0 ] && usage

case $1 in
    -*)
	;;
    *)
	BUILDTARBALL=Yes
	BUILDRPM=Yes
	BUILDHTML=Yes
	BUILDXML=Yes
	BUILDRPM=Yes
	BUILDCORE=Yes
	BUILDSTD=Yes
	BUILDLITE=Yes
	BUILDINIT=Yes
	BUILD6=Yes
	BUILD6LITE=Yes

	done=Yes
	;;
esac

while [ -z "$done" ]; do
    [ $# -eq 0 ] && break

    option=$1
    case $option in
	-*)
	    shift
	    option=${option#-}

	    [ -z "$option" ] && done=Yes && break

	    while [ -n "$option" ]; do
		case $option in
		    t*)
			BUILDTARBALL=Yes
			option=${option#t}
			;;
		    r*)
			BUILDTARBALL=Yes
			BUILDRPM=Yes
			option=${option#r}
			;;
		    h*)
			BUILDHTML=Yes
			option=${option#h}
			;;
		    x*)
			BUILDXML=Yes
			option=${option#x}
			;;
		    c*)
			BUILDCORE=Yes
			option=${option#c}
			;;
		    s*)
			BUILDSTD=Yes
			option=${option#s}
			;;
		    6*)
			BUILD6=Yes
			option=${option#6}
			;;
		    l*)
			BUILDLITE=Yes
			option=${option#l}
			;;
		    i*)
			BUILDINIT=Yes
			option=${option#i}
			;;
		    L*)
			BUILD6LITE=Yes
			option=${option#L}
			;;
		    s*)
			BUILDSRC=Yes
			option=${option#s}
			;;
	    	    *)
			usage
			;;
		esac
	    done
	    ;;
	*)
	    done=Yes
	    ;;
    esac
done

echo "Arguments are $*"

case $# in
    1)
	;;
    2)
	OLDVERSION=$2
	;;
    *)
	usage
	;;
esac

VERSION=$1
BASEVERSION=$1

LOGFILE=$LOGDIR/shorewall_build_${VERSION}.log
touch $LOGFILE
progress_message "Build of Shorewall $VERSION on $(date)"

case $VERSION in
     [45].[45].*.*)
	XMLPROJ="docs-4.4"

	PATCHRELEASE=Yes
	PATCHNUM=${VERSION##*.}
	BASEVERSION=${VERSION%.*}
	SVNBRANCH="Shorewall"
	SVN6BRANCH="Shorewall6"
	LITESVNBRANCH="Shorewall-lite"
	INITSVNBRANCH="Shorewall-init"
	LITE6SVNBRANCH="Shorewall6-lite"
	DOCTAG="docs"
	;;
     [45].[45].*)
	XMLPROJ="docs-4.4"

	SVNBRANCH="Shorewall"
	SVN6BRANCH="Shorewall6"
	LITESVNBRANCH="Shorewall-lite"
	INITSVNBRANCH="Shorewall-init"
	LITE6SVNBRANCH="Shorewall6-lite"
	DOCTAG="docs"
	;;
   *)
	echo "Unsupported Version: $VERSION"
	exit 2
	;;
esac

[ -d $DIR ] || { echo "Directory $DIR does not exist or is unaccessible" ; exit 2 ; }

progress_message "Distribution directory is $DIR"

cd $DIR

[ -f BUILDDIRECTORY ] || { echo "ERROR: $DIR does not appear to be a Shorewall build directory"; exit 1; }

[ -n "$BUILDCORE" ] && SHOREWALLDIR=shorewall-${VERSION} || SHOREWALLDIR=shorewall
SHOREWALL6DIR=shorewall6-${VERSION}
SHOREWALLLITEDIR=shorewall-lite-${VERSION}
SHOREWALLINITDIR=shorewall-init-${VERSION}
LITE6DIR=shorewall6-lite-${VERSION}
TARBALL=shorewall-${VERSION}.tgz
TARBALL6=shorewall6-${VERSION}.tgz
LITETARBALL=shorewall-lite-${VERSION}.tgz
INITTARBALL=shorewall-init-${VERSION}.tgz
LITE6TARBALL=shorewall6-lite-${VERSION}.tgz

case $VERSION in
    *Beta*|*RC*)
    	BASEVERSION=${VERSION%-*}
	RPMNAME=shorewall-${BASEVERSION}-0${VERSION#*-}.noarch.rpm
	RPM6NAME=shorewall6-${BASEVERSION}-0${VERSION#*-}.noarch.rpm
	LITERPMNAME=shorewall-lite-${BASEVERSION}-0${VERSION#*-}.noarch.rpm
	INITRPMNAME=shorewall-init-${BASEVERSION}-0${VERSION#*-}.noarch.rpm
	LITE6RPMNAME=shorewall6-lite-${BASEVERSION}-0${VERSION#*-}.noarch.rpm
	;;
    *.*.*.*)
        #
        # Patch Release
        #
        RPMNAME=shorewall-${BASEVERSION}-${PATCHNUM}.noarch.rpm
        RPM6NAME=shorewall6-${BASEVERSION}-${PATCHNUM}.noarch.rpm
        LITERPMNAME=shorewall-lite-${BASEVERSION}-${PATCHNUM}.noarch.rpm
        INITRPMNAME=shorewall-init-${BASEVERSION}-${PATCHNUM}.noarch.rpm
        LITE6RPMNAME=shorewall6-lite-${BASEVERSION}-${PATCHNUM}.noarch.rpm
        ;;
     *)
	#
	# Normal Release
	#
	RPMNAME=shorewall-${VERSION}-0base.noarch.rpm
	RPM6NAME=shorewall6-${VERSION}-0base.noarch.rpm
	LITERPMNAME=shorewall-lite-${VERSION}-0base.noarch.rpm
	INITRPMNAME=shorewall-init-${VERSION}-0base.noarch.rpm
	LITE6RPMNAME=shorewall6-lite-${VERSION}-0base.noarch.rpm
	;;
esac

HTMLDIR=shorewall-docs-html-$VERSION

if [ -n "${BUILDTARBALL}${BUILDRPM}" ]; then
    report "Shorewall directory is $DIR/$SHOREWALLDIR"
    report "Shorewall6 directory is $DIR/$SHOREWALL6DIR"
    report "Shorewall Lite directory is $DIR/$SHOREWALLLITEDIR"
    report "Shorewall Init directory is $DIR/$SHOREWALLINITDIR"
    report "Shorewall6 Lite directory is $DIR/$LITE6DIR"
    report "SVN tag is $SVNBRANCH"
    report "SVN6 tag is $SVN6BRANCH"
    report "Lite SVN tag is $LITESVNBRANCH"
    report "Init SVN tag is $INITSVNBRANCH"
    report "Lite6 SVN tag is $LITE6SVNBRANCH"
	
    if [ -n "$BUILDTARBALL" ]; then
	report "TARBALL is $TARBALL"
	report "TARBALL6 is $TARBALL6"
	report "LITETARBALL is $LITETARBALL"
	report "INITTARBALL is $INITTARBALL"
	report "LITE6TARBALL is $LITE6TARBALL"
    fi

    if [ -n "$BUILDRPM" ]; then
	report "RPM is $RPMNAME"
	[ -n "$RPM6NAME" ] && report "RPM6 is $RPM6NAME"
	report "LITERPM is $LITERPMNAME"
	report "INITRPM is $INITRPMNAME"
	[ -n "LITE6RPMNAME" ] && report "LITE6RPM is $LITE6RPMNAME"
    fi
fi

[ -n "$BUILDHTML" ] && report "HTML Directory is $HTMLDIR"

[ -n "$RPM6NAME" ]     || BUILD6=
[ -n "$LITE6RPMNAME" ] || BUILD6LITE=

if [ -n "${BUILDTARBALL}${BUILDRPM}" ]; then

    rm -rf $SHOREWALLDIR

    [ -n "$BUILD6" ]      && rm -rf $SHOREWALL6DIR
    [ -n "$BUILDLITE" ]   && rm -rf $SHOREWALLLITEDIR
    [ -n "$BUILDINIT" ]   && rm -rf $SHOREWALLINITDIR
    [ -n "$BUILD6LITE" ]  && rm -rf $LITE6DIR

    do_export $SVNBRANCH $SHOREWALLDIR

    if [ ! -f $SHOREWALLDIR/changelog.txt ]; then
	rm -rf $RELEASEDIR
	mkdir -p $RELEASEDIR
	cd $RELEASEDIR
	[ -f $GIT/release/.git/refs/heads/$BASEVERSION ] && BRANCH=$BASEVERSION || BRANCH=master
	progress_message "Exporting release files from Git branch $BRANCH..." && do_or_die "git --git-dir=$GIT/release/.git archive --format=tar $BRANCH | tar -xf - >> $LOGFILE 2>&1"
	cd ..
	do_or_die cp $RELEASEDIR/changelog.txt $RELEASEDIR/releasenotes.txt $RELEASEDIR/known_problems.txt $RELEASEDIR/shorewall.spec $SHOREWALLDIR/
    fi

    setperlversions

    do_or_die cp $SHOREWALLDIR/changelog.txt $SHOREWALLDIR/releasenotes.txt $SHOREWALLDIR/known_problems.txt .

    if [ -n "$BUILD6" ]; then
	do_export $SVN6BRANCH $SHOREWALL6DIR
	[ -f $SHOREWALL6DIR/shorewall6.spec ] || do_or_die cp $RELEASEDIR/shorewall6.spec $SHOREWALL6DIR
	do_or_die "cp $SHOREWALLDIR/changelog.txt  $SHOREWALLDIR/releasenotes.txt $SHOREWALL6DIR >> $LOGFILE 2>&1"
    fi
 
    if [ -n "$BUILDINIT" ]; then
	do_export $INITSVNBRANCH  $SHOREWALLINITDIR
	[ -f $SHOREWALLINITDIR/shorewall-init.spec ] || do_or_die cp $RELEASEDIR/shorewall-init.spec $SHOREWALLINITDIR
	do_or_die "cp $SHOREWALLDIR/changelog.txt  $SHOREWALLDIR/releasenotes.txt $SHOREWALLINITDIR >> $LOGFILE 2>&1"
    fi
    	
    if [ -n "$BUILDLITE" ]; then
	do_export $LITESVNBRANCH  $SHOREWALLLITEDIR
	[ -f $SHOREWALLLITEDIR/shorewall-lite.spec ] || do_or_die cp $RELEASEDIR/shorewall-lite.spec $SHOREWALLLITEDIR
	do_or_die "cp $SHOREWALLDIR/modules* $SHOREWALLLITEDIR >> $LOGFILE 2>&1"
	do_or_die "cp $SHOREWALLDIR/helpers $SHOREWALLLITEDIR >> $LOGFILE 2>&1"
	do_or_die "cp $SHOREWALLDIR/lib.base $SHOREWALLLITEDIR >> $LOGFILE 2>&1"
	do_or_die "cp $SHOREWALLDIR/lib.cli $SHOREWALLLITEDIR >> $LOGFILE 2>&1"

	if [ -f $SHOREWALLDIR/lib.core ]; then
	    do_or_die "cp $SHOREWALLDIR/lib.core $SHOREWALLLITEDIR >> $LOGFILE 2>&1"
	fi

	if [ ! -f $SHOREWALLLITEDIR/shorewall-lite ]; then
	    do_or_die "cp $SHOREWALLDIR/shorewall $SHOREWALLLITEDIR/shorewall-lite >> $LOGFILE 2>&1"
	fi

	eval sed -i \'s\|g_program:=shorewall\|g_program:=shorewall-lite\|\' $SHOREWALLLITEDIR/lib.base
	
	if [ -f $SHOREWALLDIR/wait4ifup ]; then
	    do_or_die "cp $SHOREWALLDIR/wait4ifup $SHOREWALLLITEDIR >> $LOGFILE 2>&1"
	fi

	do_or_die "cp $SHOREWALLDIR/changelog.txt  $SHOREWALLDIR/releasenotes.txt $SHOREWALLLITEDIR >> $LOGFILE 2>&1"
    fi

    if [ -n "$BUILD6LITE" ]; then
	do_export $LITE6SVNBRANCH  $LITE6DIR
	[ -f $LITE6DIR/shorewall6-lite.spec ] || do_or_die cp $RELEASEDIR/shorewall6-lite.spec $LITE6DIR
	do_or_die "cp $SHOREWALL6DIR/modules*  $LITE6DIR >> $LOGFILE 2>&1"
	do_or_die "cp $SHOREWALL6DIR/helpers   $LITE6DIR >> $LOGFILE 2>&1"

	if [ -f $SHOREWALL6DIR/lib.cli ]; then
	    do_or_die "cp $SHOREWALL6DIR/lib.base  $LITE6DIR >> $LOGFILE 2>&1"
	    do_or_die "cp $SHOREWALL6DIR/lib.cli   $LITE6DIR >> $LOGFILE 2>&1"
	else
	    do_or_die "cp $SHOREWALLDIR/lib.base  $LITE6DIR >> $LOGFILE 2>&1"
	    do_or_die "cp $SHOREWALLDIR/lib.cli   $LITE6DIR >> $LOGFILE 2>&1"

	    eval sed -i \'s\|g_program:=shorewall\|g_program:=shorewall6-lite\|\' $LITE6DIR/lib.base
	    eval sed -i \'s\|g_family:=4\|g_family:=6\|\' $LITE6DIR/lib.base
	fi

	if [ ! -f $LITE6DIR/shorewall6-lite ]; then
	    do_or_die "cp $SHOREWALLDIR/shorewall $LITE6DIR/shorewall6-lite"
	fi

	if [ -f $SHOREWALL6DIR/lib.core ]; then
	    do_or_die "cp $SHOREWALL6DIR/lib.core $LITE6DIR >> $LOGFILE 2>&1"
	elif [ -f $SHOREWALLDIR/lib.core ]; then
	    do_or_die "cp $SHOREWALLDIR/lib.core $LITE6DIR >> $LOGFILE 2>&1"
	    do_or_die "cp $SHOREWALLLITEDIR/lib.cli-lite $LITE6DIR >> $LOGFILE 2>&1"
	fi

	do_or_die "cp $SHOREWALL6DIR/wait4ifup $LITE6DIR >> $LOGFILE 2>&1"
	do_or_die "cp $SHOREWALLDIR/changelog.txt  $SHOREWALLDIR/releasenotes.txt $LITE6DIR >> $LOGFILE 2>&1"
    fi

    if [ -n "$BUILDCORE" ]; then
	fgrep VERSION=$VERSION $SHOREWALLDIR/install.sh > /dev/null 2>&1     || fatal_error "install.sh has wrong version"
	fgrep VERSION=$VERSION $SHOREWALLDIR/uninstall.sh > /dev/null 2>&1   || fatal_error "uninstall.sh has wrong version"

	if [ -d $SHOREWALLDIR/Perl ]; then
	    [ $(eval perl -e "'use lib \"$SHOREWALLDIR/Perl\"; use Shorewall::Config qw(:internal); Shorewall::Config::initialize(4); print \"\$globals{VERSION}\n\"'") = $VERSION ] || \
		fatal_error "Perl Config.pm has wrong version"
	else
	    [ $(eval perl -e "'use lib \"$SHOREWALLDIR\"; use Shorewall::Config qw(:internal); Shorewall::Config::initialize(4); print \"\$globals{VERSION}\n\"'") = $VERSION ] || \
		fatal_error "Perl Config.pm has wrong version"
	fi
    fi

    if [ -n "$BUILD6" ]; then
	fgrep VERSION=$VERSION $SHOREWALL6DIR/install.sh > /dev/null 2>&1     || fatal_error "6 install.sh has wrong version"
	fgrep VERSION=$VERSION $SHOREWALL6DIR/uninstall.sh > /dev/null 2>&1   || fatal_error "6 uninstall.sh has wrong version"
    fi

    if [ -n "$BUILDLITE" ]; then
	fgrep VERSION=$VERSION $SHOREWALLLITEDIR/install.sh > /dev/null 2>&1     || fatal_error "Lite install.sh has wrong version"
	fgrep VERSION=$VERSION $SHOREWALLLITEDIR/uninstall.sh > /dev/null 2>&1   || fatal_error "Lite uninstall.sh has wrong version"
    fi

    if [ -n "$BUILDINIT" ]; then
	fgrep VERSION=$VERSION $SHOREWALLINITDIR/install.sh > /dev/null 2>&1     || fatal_error "Init install.sh has wrong version"
	fgrep VERSION=$VERSION $SHOREWALLINITDIR/uninstall.sh > /dev/null 2>&1   || fatal_error "Init uninstall.sh has wrong version"
    fi

    if [ -n "$BUILD6LITE" ]; then
	fgrep VERSION=$VERSION $LITE6DIR/install.sh > /dev/null 2>&1     || fatal_error "Lite 6 install.sh has wrong version"
	fgrep VERSION=$VERSION $LITE6DIR/uninstall.sh > /dev/null 2>&1   || fatal_error "Lite 6 uninstall.sh has wrong version"
    fi

    if [ -n "$BUILDCORE" ]; then
	cd $SHOREWALLDIR
	do_export $SAMPLESTAG Samples
	do_manpages '' ''
	do_configfiles $PWD/configfiles $PWD/manpages ''

	progress_message "Annotating Sample .conf files..."

	manpagedir=$PWD/manpages
	
	pushd Samples > /dev/null

	for dir in *; do
	    if [ -d $dir ]; then
		cd $dir
		progress_message "*** $PWD ***"
		do_configfiles $PWD $manpagedir ''
		cd ..
	    fi
	done

	popd > /dev/null

	rm -f manpages/*.xml
	    
	cd $DIR
    fi

    if [ -n "$BUILD6" ]; then
	cd $SHOREWALL6DIR
	do_export $SAMPLES6TAG Samples6
	do_manpages 6 6
	do_configfiles $PWD/configfiles $PWD/manpages 6
	
	progress_message "Annotating Sample .conf files..."

	manpagedir=$PWD/manpages

	pushd Samples6 2> /dev/null

	for dir in *; do
	    if [ -d $dir ]; then
		cd $dir
		progress_message "*** $PWD ***"
		do_configfiles $PWD $manpagedir 6
		cd ..
	    fi
	done

	popd > /dev/null

	rm -f manpages/*.xml

	cd $DIR
    fi

    if [ -n "$BUILDLITE" ]; then
	cd $SHOREWALLLITEDIR
	do_manpages -lite ''
	cd $DIR 
    fi	

    if [ -n "$BUILD6LITE" ]; then
	cd $LITE6DIR
	do_manpages 6-lite 6
	cd $DIR 
    fi	

    [ -n "$BUILDCORE" ] && do_or_die "rm -rf $SHOREWALLDIR/debian"
    [ -n "$BUILD6" ]      && do_or_die "rm -rf $SHOREWALL6DIR/debian"
    [ -n "$BUILDLITE" ]   && do_or_die "rm -rf $SHOREWALLLITEDIR/debian"
    [ -n "$BUILDINIT" ]   && do_or_die "rm -rf $SHOREWALLINITDIR/debian"
    [ -n "$BUILD6LITE" ]  && do_or_die "rm -rf $LITE6DIR/debian"

    if [ -n "$BUILDTARBALL" ]; then
	if [ -n "$BUILDCORE" ]; then	
	    progress_message "Creating $DIR/$TARBALL..."
	    rm -f $SHOREWALLDIR/*.diff
	    do_or_die "tar -zcvf $TARBALL $SHOREWALLDIR >> $LOGFILE 2>&1"
	    do_or_die "tar -jcvf shorewall-${VERSION}.tar.bz2 $SHOREWALLDIR >> $LOGFILE 2>&1"
	fi

	if [ -n "$BUILD6" ]; then	
	    progress_message "Creating $DIR/$TARBALL6..."
	    rm -f $SHOREWALL6DIR/*.diff
	    do_or_die "tar -zcvf $TARBALL6 $SHOREWALL6DIR >> $LOGFILE 2>&1"
	    do_or_die "tar -jcvf shorewall6-${VERSION}.tar.bz2 $SHOREWALL6DIR >> $LOGFILE 2>&1"
	fi

	if [ -n "$BUILDLITE" ]; then	
	    progress_message "Creating $DIR/$LITETARBALL..."
	    rm -f $SHOREWALLLITEDIR/*.diff
	    do_or_die "tar -zcvf $LITETARBALL $SHOREWALLLITEDIR >> $LOGFILE 2>&1"
	    do_or_die "tar -jcvf shorewall-lite-${VERSION}.tar.bz2 $SHOREWALLLITEDIR >> $LOGFILE 2>&1"
	fi
    	
	if [ -n "$BUILDINIT" ]; then	
	    progress_message "Creating $DIR/$INITTARBALL..."
	    rm -f $SHOREWALLINITDIR/*.diff
	    do_or_die "tar -zcvf $INITTARBALL $SHOREWALLINITDIR >> $LOGFILE 2>&1"
	    do_or_die "tar -jcvf shorewall-init-${VERSION}.tar.bz2 $SHOREWALLINITDIR >> $LOGFILE 2>&1"
	fi
    	
	if [ -n "$BUILD6LITE" ]; then	
	    progress_message "Creating $DIR/$LITE6TARBALL..."
	    rm -f $LITE6DIR/*.diff
	    do_or_die "tar -zcvf $LITE6TARBALL $LITE6DIR >> $LOGFILE 2>&1"
	    do_or_die "tar -jcvf shorewall6-lite-${VERSION}.tar.bz2 $LITE6DIR >> $LOGFILE 2>&1"
	fi    	
    fi

    if [ -n "$BUILDRPM" ]; then
	[ -n "$BUILDCORE" ] && do_buildanrpm $RPMNAME      shorewall       $SHOREWALLDIR
	[ -n "$BUILD6" ]      && do_buildanrpm $RPM6NAME     shorewall6	     $SHOREWALL6DIR
	[ -n "$BUILDLITE" ]   && do_buildanrpm $LITERPMNAME  shorewall-lite  $SHOREWALLLITEDIR
	[ -n "$BUILDINIT" ]   && do_buildanrpm $INITRPMNAME  shorewall-init  $SHOREWALLINITDIR
	[ -n "$BUILD6LITE" ]  && do_buildanrpm $LITE6RPMNAME shorewall6-lite $LITE6DIR
    fi
fi

if [ -n "${BUILDXML}${BUILDHTML}" ]; then
    rm -rf $XMLPROJ
    rm -rf shorewall-docs-xml-$VERSION

    do_export $DOCTAG $XMLPROJ
    do_or_die mv $XMLPROJ shorewall-docs-xml-$VERSION

    rm -f shorewall-docs-xml-$VERSION/images/*.vsd
    rm -f shorewall-docs-xml-$VERSION/images/~*
    rm -f shorewall-docs-xml-$VERSION/images/*.JPG
    rm -f shorewall-docs-xml-$VERSION/images/publish
    rm -f shorewall-docs-xml-$VERSION/images/Thumbs.db

    cd shorewall-docs-xml-$VERSION

    do_export_manpages Shorewall manpages
    do_export_manpages Shorewall6 manpages6
    do_export_manpages Shorewall-lite manpages-lite
    do_export_manpages Shorewall6-lite manpages6-lite

    do_or_die cp -a manpages-lite/* manpages
    do_or_die cp -a manpages6-lite/* manpages6

    do_or_die rm -rf manpages-lite
    do_or_die rm -rf manpages6-lite

    cd $DIR

    if [ -n "$BUILDXML" ]; then
	progress_message "Creating $DIR/shorewall-docs-xml-$VERSION tarballs"
        tar -zcvf shorewall-docs-xml-$VERSION.tgz shorewall-docs-xml-$VERSION >> $LOGFILE 2>&1
        tar -jcvf shorewall-docs-xml-$VERSION.tar.bz2 shorewall-docs-xml-$VERSION >> $LOGFILE 2>&1 || true
    fi

    if [ -n "$BUILDHTML" ]; then
	progress_message "Building $HTMLDIR ..."

	rm -rf $HTMLDIR

	do_or_die mkdir $HTMLDIR
	do_or_die mkdir $HTMLDIR/images
	#
	# The original HTML documents were created using MS FrontPage and used
	# the .htm suffix. The remainder use the .html suffix.
	#
	HTMFILES="
	    6to4.htm
	    blacklisting_support.htm
	    configuration_file_basics.htm
	    CorpNetwork.htm
	    dhcp.htm
	    Documentation.htm
	    errata.htm
	    fallback.htm
	    FAQ.htm
	    GnuCopyright.htm
	    Install.htm
	    IPIP.htm
	    IPSEC.htm
	    kernel.htm
	    myfiles.htm
	    NAT.htm
	    ports.htm
	    PPTP.htm
	    ProxyARP.htm
	    quotes.htm
	    samba.htm
	    shorewall_extension_scripts.htm
	    shorewall_features.htm
	    shorewall_mirrors.htm
	    shorewall_prerequisites.htm
	    shorewall_quickstart_guide.htm
	    shorewall_setup_guide_fr.htm
	    shorewall_setup_guide.htm
	    Shorewall_sfindex_frame.htm
	    standalone.htm
	    starting_and_stopping_shorewall.htm
	    support.htm
	    three-interface.htm
	    traffic_shaping.htm
	    troubleshoot.htm
	    two-interface.htm
	    upgrade_issues.htm
	    VPN.htm
	    whitelisting_under_shorewall.htm"

	NOTOC="
            Documentation_Index.xml
            ECN.xml
            fallback.xml
            GettingStarted.xml
            IPP2P.xml
            ping.xml
            ProxyARP.xml
            Shorewall_Doesnt.xml
            shorewall_features.xml
            shorewall_prerequisites.xml
            SimpleBridge.xml"

	SHORTTOC="
            FAQ.xml"

	for file in shorewall-docs-xml-$VERSION/*.xml; do
	    a=$(basename $file)
	    b=${a%.*}
	    list_search $b.htm $HTMFILES && b=$b.htm || b=$b.html
	    f="shorewall-docs-html-$VERSION/$b"
	    list_search $1 $NOTOC && GENTOC="--stringparam generate.toc ''" || GENTOC=

	    DEPTH=3

	    [ -z "$GENTOC" ] && list_search $1 $SHORTTOC && DEPTH=1

	    case $file in
		*_ru.xml)
		    LANGUAGE="--stringparam l10n.gentext.default.language ru"
		    ;;
		*_fr.xml)
		    LANGUAGE="--stringparam l10n.gentext.default.language fr"
		    ;;
		*)
		    LANGUAGE=
		    ;;
	    esac

	    report "Converting $DIR/$file from XML to HTML ($DIR/$f) ..."

	    do_or_die xsltproc --output $f --stringparam html.stylesheet html.css --stringparam ulink.target _self $GENTOC -param toc.section.depth $DEPTH $STYLESHEET $file
	done

	for f in shorewall-docs-xml-$VERSION/manpages/*.xml shorewall-docs-xml-$VERSION/manpages6/*.xml; do
	    case $f in
		*template.xml)
		    ;;
		*)
		    progress_message "Generating HTML from $f..."
		    do_or_die xsltproc --output ${f%.xml}.html --stringparam html.stylesheet html.css --stringparam ulink.target _self -param toc.section.depth 3 $STYLESHEET $f
		    ;;
	    esac
	done

	progress_message "Copying manpages to $DIR/$HTMLDIR/images ..."

	do_or_die mkdir $HTMLDIR/manpages
	do_or_die cp -a shorewall-docs-xml-$VERSION/manpages/*.html $HTMLDIR/manpages/
	do_or_die rm -f shorewall-docs-xml-$VERSION/manpages/*.html

	do_or_die mkdir $HTMLDIR/manpages6
	do_or_die cp -a shorewall-docs-xml-$VERSION/manpages6/*.html $HTMLDIR/manpages6/
	do_or_die rm -f shorewall-docs-xml-$VERSION/manpages6/*.html

	progress_message "Copying images to $DIR/$HTMLDIR/images ..."

	do_or_die cp -a shorewall-docs-xml-$VERSION/images/*.png $HTMLDIR/images
	do_or_die cp -a shorewall-docs-xml-$VERSION/images/*.gif $HTMLDIR/images
	do_or_die cp -a shorewall-docs-xml-$VERSION/images/*.jpg $HTMLDIR/images
	do_or_die cp -a shorewall-docs-xml-$VERSION/*.css $HTMLDIR

	do_or_die ln -s Documentation_Index.html shorewall-docs-html-$VERSION/index.html

	progress_message "Creating $DIR/shorewall-docs-html-$VERSION tarballs ..."

	do_or_die "tar -zcvf shorewall-docs-html-$VERSION.tgz shorewall-docs-html-$VERSION >> $LOGFILE 2>&1"
	do_or_die "tar -jcvf shorewall-docs-html-$VERSION.tar.bz2 shorewall-docs-html-$VERSION >> $LOGFILE 2>&1"
    fi
fi

if [ -n "$BUILDSRC" ]; then
    progress_message "Building Master Source Package shorewall-src-$VERSION.tar.bz2"

    SHOREWALLSRC=shorewall-$BASEVERSION
    SHOREWALL6SRC=shorewall6-$BASEVERSION
    LITESRC=shorewall-lite-$BASEVERSION
    LITE6SRC=shorewall6-lite-$BASEVERSION
    XMLSRC=shorewall-docs-xml-$BASEVERSION
    HTMLSRC=shorewall-docs-html-$BASEVERSION

    if [ $VERSION != $BASEVERSION ]; then
	[ -d shorewall-$VERSION ]            && SHOREWALLSRC=shorewall-$VERSION
	[ -d shorewall6-$VERSION ]           && SHOREWALL6SRC=shorewall6-$VERSION
    	[ -d shorewall-lite-$VERSION ]       && LITESRC=shorewall-lite-$VERSION
    	[ -d shorewall6-lite-$VERSION ]      && LITE6SRC=shorewall-lite-$VERSION
    	[ -d shorewall-docs-xml-$VERSION ]   && XMLSRC=shorewall-docs-xml-$VERSION
    	[ -d shorewall-docs-html-$VERSION ]  && HTMLSRC=shorewall-docs-html-$VERSION
    fi

    SRCDIR=shorewall-src-$VERSION

    rm -rf $SRCDIR

    do_or_die "mkdir $SRCDIR"

    do_or_die "cp -a $SHOREWALLSRC  $SRCDIR/shorewall/" 
    do_or_die "cp -a $SHOREWALL6SRC $SRCDIR/shorewall6/" 
    do_or_die "cp -a $LITESRC       $SRCDIR/shorewall-lite/" 
    do_or_die "cp -a $LITE6SRC      $SRCDIR/shorewall6-lite/" 

    do_or_die "tar -zcvf shorewall-src-$VERSION.tar.bz2 $SRCDIR >> $LOGFILE 2>&1"
fi    

progress_message "Creating md5sums and sha1sums"

rm -f $VERSION.md5sums $VERSION.sha1sums

#
# The following rather awkward algorithm gets around the problem of builds that don't
# include the RPM
#
case $VERSION in
    *Beta*|*RC*)
	if [ -n "$BUILDCORE" ]; then		
	    do_or_die "md5sum shorewall-core-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.md5sums"
	    do_or_die "sha1sum shorewall-core-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.sha1sums"
	fi

	if [ -n "$BUILDSTD" ]; then		
	    do_or_die "md5sum shorewall-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.md5sums"
	    do_or_die "sha1sum shorewall-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.sha1sums"
	fi

	if [ -n "$BUILD6" ]; then		
	    do_or_die "md5sum shorewall6-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.md5sums"
	    do_or_die "sha1sum shorewall6-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.sha1sums"
	fi

	if [ -n "$BUILDLITE" ]; then		
	    do_or_die "md5sum shorewall-lite-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.md5sums"
	    do_or_die "sha1sum shorewall-lite-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.sha1sums"
	fi

	if [ -n "$BUILDINIT" ]; then		
	    do_or_die "md5sum shorewall-init-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.md5sums"
	    do_or_die "sha1sum shorewall-init-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.sha1sums"
	fi

	if [ -n "$BUILD6LITE" ]; then		
	    do_or_die "md5sum shorewall6-lite-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.md5sums"
	    do_or_die "sha1sum shorewall6-lite-${BASEVERSION}-0${VERSION#*-}.noarch.rpm >> $VERSION.sha1sums"
	fi
	;;
    *.*.*.*)
	[ -f $RPMNAME ]      && do_or_die "md5sum $RPMNAME >> $VERSION.md5sums"      && do_or_die "sha1sum $RPMNAME >> $VERSION.sha1sums"
	[ -f $RPM6NAME ]     && do_or_die "md5sum $RPM6NAME >> $VERSION.md5sums"     && do_or_die "sha1sum $RPM6NAME >> $VERSION.sha1sums"
	[ -f $LITERPMNAME ]  && do_or_die "md5sum $LITERPMNAME >> $VERSION.md5sums"  && do_or_die "sha1sum $LITERPMNAME >> $VERSION.sha1sums"
	[ -f $INITRPMNAME ]  && do_or_die "md5sum $INITRPMNAME >> $VERSION.md5sums"  && do_or_die "sha1sum $INITRPMNAME >> $VERSION.sha1sums"
	[ -f $LITE6RPMNAME ] && do_or_die "md5sum $LITE6RPMNAME >> $VERSION.md5sums" && do_or_die "sha1sum $LITE6RPMNAME >> $VERSION.sha1sums"
	;;
esac

if [ -n "$OLDVERSION" ]; then
    
    progress_message "Creating patch-$VERSION ..."

    [ -d shorewall-$VERSION ]        && [ -d shorewall-$OLDVERSION ]        && diff -Naurdw -X $(dirname $0)/exclude.txt shorewall-$OLDVERSION        shorewall-$VERSION       > patch-$VERSION        || true
    [ -d shorewall6-$VERSION ]       && [ -d shorewall6-$OLDVERSION ]       && diff -Naurdw -X $(dirname $0)/exclude.txt shorewall6-$OLDVERSION       shorewall6-$VERSION      > patch-6-$VERSION      || true
    [ -d shorewall-lite-$VERSION ]   && [ -d shorewall-lite-$OLDVERSION ]   && diff -Naurdw -X $(dirname $0)/exclude.txt shorewall-lite-$OLDVERSION   shorewall-lite-$VERSION  > patch-lite-$VERSION   || true
    [ -d shorewall-init-$VERSION ]   && [ -d shorewall-init-$OLDVERSION ]   && diff -Naurdw -X $(dirname $0)/exclude.txt shorewall-init-$OLDVERSION   shorewall-init-$VERSION  > patch-init-$VERSION   || true
    [ -d shorewall6-lite-$VERSION ]  && [ -d shorewall6-lite-$OLDVERSION ]  && diff -Naurdw -X $(dirname $0)/exclude.txt shorewall6-lite-$OLDVERSION  shorewall6-lite-$VERSION > patch-6-lite-$VERSION || true
    
fi

progress_message "Shorewall $VERSION Build complete - $(date)"
