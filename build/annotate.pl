#! /usr/bin/perl
#
#  Annotate *.conf -- (C) 2011 -- Tom Eastep (teastep@shorewall.net)
#
#     This program is under GPL [http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt]
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of Version 2 of the GNU General Public License
#	as published by the Free Software Foundation.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Usage:
#
#   Annotate <.conf file> <text version of manpage>
#
#   The annotated config file is written to standard out
########################################################################################
use strict;

my ( $confname, $txtname ) = @ARGV;

my ( $conf, $txt , $mac );

my %options;

my %deprecated;

sub fatal_error {
    die "   ERROR: @_";
}

sub add_option( ;$ ) {
    my $option = shift;
    my @lines;
    my $deprecated;
    my $result = 0;

    unless ( defined $option ) {
	if ( /^\s*(\w+?)=/ ) {
	    $option = $1;
	} else {
	    die '   ERROR: Internal error in add_option()';
	}
    }

    push @lines, $_;

    while ( <$txt> ) {
	last if /^(?:   )?FILE/;

	$deprecated ||= /Deprecated/;

	if ( $mac ) {
	    $result = 1, last if /^\s{0,3}(\w+?)=/;
	    #
	    # Compensate for bugs in MacPorts xmlto
	    #
	    s/^/          / unless /^ /;
	    s/^/       /    unless /^    /;
	    s/^/    /       unless /^     /; 
	    s/^/     /      unless /^       /;
	    s/^/  /         unless /^          /;
	} else {
	    $result = 1, last if /^(?:   )?(\w+?)=/;
	}

	push @lines, $_;
    }

    $options{$option}    = \@lines;
    $deprecated{$option} = 1 if $deprecated;
    
    $result;
}

sub annotate_option( ;$ ) {
    my $option = shift;

    unless ( defined $option ) {
	print $_;
	if ( /^(\w+?)=/ ) {
	    $option = $1;
	} else {
	    die '   ERROR: Internal error in print_option()';
	}
    }

    fatal_error "No manpage text for $option"      unless $options{$option};
    fatal_error "The $option option is deprecated" if $deprecated{$option};

    print "#\n";

    print "# $_" for @{$options{$option}};

    delete $options{$option};
}

sub next_option {
    while ( <$conf> ) {
	return 1 if /^(?:\w+?)=/;
	print $_ unless /^\s*$/;
    }

    0;
}

fatal_error( "usage: $0 <config file> <text manpage>" ) unless @ARGV == 2;

chomp ( my $uname = `uname` );

$mac = ( $uname eq 'Darwin' );


open $conf, '<', $confname or fatal_error "Unable to open $confname: $!";
open $txt,  '<', $txtname  or fatal_error "Unable to open $txtname: $!";

while ( <$txt> ) {
    last if /^OPTIONS/;
}

add_option( 'OPTIONS' );

1 while add_option();

close $txt;

while ( <$conf> ) {
    next if /^\w*$/;
    last if /^STARTUP_ENABLED=/;
    print $_;
}

annotate_option('OPTIONS');

while (1) {
    annotate_option();
    last unless next_option;
}

close $conf;

delete $options{$_} for keys %deprecated;

for ( qw/IPTABLES IP6TABLES DETECT_DNAT_IPADDRS NULL_ROUTE_RFC1918 RETAIN_ALIASES LOG_MARTIANS ARPTABLES SAVE_ARPTABLES MULTICAST MAPOLDACTIONS ADD_SNAT_ALIASES ROUTE_FILTER DOCKER DOCKER_BRIDGE DISABLE_IPV6 ADD_IP_ALIASES LOAD_HELPERS_ONLY/ ) {
    delete $options{$_};
}

fatal_error "$confname does not set option $_" for keys %options;






