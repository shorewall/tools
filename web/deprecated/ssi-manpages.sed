# This sed script inserts the Apache SSI directives in the correct place
# for each generated documentation file.
# The files included by these SSI directives provide the necessary navigation
# and associated elements for the site.
# The manpages end up in the manpages/ or manpages6/ subdirectory, so the
# include files need to be referenced one level higher in the directory tree.
s/<body>/\
  <body>\
    <!--#include virtual="\.\.\/header.inc" -->\
    <!--#include virtual="\.\.\/sidebar_left.inc" -->\
    <!--#include virtual="\.\.\/div_main_open.inc" -->\
    <!--#include virtual="\.\.\/div_content_open.inc" -->\
/
s/<\/body>/\
    <!--#include virtual="\.\.\/div_content_close.inc" -->\
    <!--#include virtual="\.\.\/footer.inc" -->\
    <!--#include virtual="\.\.\/div_main_close.inc" -->\
    <!--#include virtual="\.\.\/sidebar_right.inc" -->\
  <\/body>\
/
