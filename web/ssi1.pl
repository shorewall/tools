#! /usr/bin/perl

use strict;
use File::HomeDir;

my $webdir = File::HomeDir->my_home .'/shorewall/web/';

sub include( @ ) {
    my $file;

    for my $filename ( @_ ) {
	open $file, '<', $webdir . $filename or die "Can't open ${webdir}${filename} @";

	while ( <$file> ) {
	    print $_;
	}

	close $file;
    }
}

while ( <> ) {
    if ( $_ =~ /include virtual="(.*)"/ ) {
	include ( $1 );
    } else {
	print;
    }
}
