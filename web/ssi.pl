#! /usr/bin/perl

use strict;
use File::HomeDir;

my $webdir = File::HomeDir->my_home .'/shorewall/web/';

sub include( @ ) {
    my $file;

    for my $filename ( @_ ) {
	open $file, '<', $webdir . $filename . '.inc' or die "Can't open ${webdir}${filename}.inc @";

	while ( <$file> ) {
	    print $_;
	}

	close $file;
    }
}

while ( <> ) {
    if ( $_ =~ /(.*<body>)(.*)/ ) {
	print "$1\n";
	include ( qw( header sidebar_left div_main_open div_content_open ) );
	print $2;
    } elsif ( $_ =~ /(.*<\/body>)(.*)/ ) {
	print "$1\n";
	include ( qw( div_content_close footer div_main_close sidebar_right ) );
	print $2;
    } else {
	print;
    }
}
